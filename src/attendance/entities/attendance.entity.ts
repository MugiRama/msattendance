import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity()
export class Attendance {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    userEmail: string;

    @Column({
        type: 'datetime',
        nullable: false,
        default: () => 'CURRENT_TIMESTAMP'
    })
    timestamp: Date;

    @Column()
    isEntry: boolean;

    @Column('double')
    latitude: number;

    @Column('double')
    longitude: number;

    @Column({ default: false })
    isAdmin: boolean;
}
