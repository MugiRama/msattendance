import { Injectable, InternalServerErrorException } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Between, Repository } from "typeorm";
import { Attendance } from "../entities/attendance.entity";
import { eachDayOfInterval } from "date-fns";

@Injectable()
export class AttendanceQueryService {
    constructor(@InjectRepository(Attendance) private attendanceRepository: Repository<Attendance>) {}

    async findAttendances(startDate: Date, endDate: Date, email: string): Promise<any> {
        console.log(`Querying from ${startDate.toISOString()} to ${endDate.toISOString()} for user ${email}`);
        try {
            const attendances = await this.attendanceRepository.find({
                where: {
                    timestamp: Between(startDate, endDate),
                    userEmail: email
                },
                order: {
                    timestamp: 'ASC'
                }
            });
            console.log(`Found ${attendances.length} records`);

            if (!attendances.length) {
                return { message: 'No hay registros para esta semana' };
            }

            const groupedByDay = attendances.reduce((acc, record) => {
                const dateKey = record.timestamp.toISOString().split('T')[0];
                if (!acc[dateKey]) {
                    acc[dateKey] = [];
                }
                acc[dateKey].push(record);
                return acc;
            }, {});

            console.log(`Grouped by Day: ${JSON.stringify(groupedByDay)}`);

            return groupedByDay;
        } catch (error) {
            console.error('Error al recuperar las asistencias:', error);
            throw new InternalServerErrorException('No se pudo recuperar la información de asistencia');
        }
    }

    async findAttendancesByEmail(startDate: Date, endDate: Date, email: string): Promise<any> {
        const daysRange = eachDayOfInterval({ start: startDate, end: endDate });
        try {
            const attendances = await this.attendanceRepository.find({
                where: {
                    timestamp: Between(startDate, endDate),
                    userEmail: email
                },
                order: {
                    timestamp: 'ASC'
                }
            });

            const attendancesByDay = daysRange.reduce((acc, day) => {
                const dateKey = day.toISOString().split('T')[0];
                acc[dateKey] = attendances.filter(a =>
                    a.timestamp.toISOString().split('T')[0] === dateKey
                );

                if (acc[dateKey].length === 0) {
                    acc[dateKey].push({
                        id: null,
                        timestamp: new Date(Date.UTC(day.getUTCFullYear(), day.getUTCMonth(), day.getUTCDate(), 0, 0, 0)),
                        isEntry: true,
                        userEmail: email,
                        isPlaceholder: true
                    });
                }

                return acc;
            }, {});

            return attendancesByDay;
        } catch (error) {
            console.error('Error al recuperar las asistencias:', error);
            throw new InternalServerErrorException('No se pudo recuperar la información de asistencia');
        }
    }
}
