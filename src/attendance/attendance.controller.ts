import { Controller, Get, Post, Body, Patch, Param, Delete, HttpException, HttpStatus, Query, UseGuards, BadRequestException, Req } from '@nestjs/common';
import { AttendanceService } from './attendance.service';
import { CreateAttendanceDto } from './dto/create-attendance.dto';
import { UpdateAttendanceDto } from './dto/update-attendance.dto';
import { AuthGuard } from './guard/authAttendance.guard';
import { Auth } from './decorators/auth.decorator';
import { Role } from 'src/common/enums/rol.enum';
import { RolesGuard } from './guard/roles.guard';
import { Roles } from './decorators/roles.decorator';
import { DateUtils } from 'src/common/date-utils';

@Controller('attendance')
@UseGuards(AuthGuard)
export class AttendanceController {
  constructor(private readonly attendanceService: AttendanceService) { }
  @Post('mark')
  @Auth(Role.USER)
  @UseGuards(AuthGuard, RolesGuard)
  async markAttendance(@Body() createAttendanceDto: CreateAttendanceDto, @Req() request: any) {
    try {
      const user = request.user; 
      return await this.attendanceService.create(createAttendanceDto, user);
    } catch (error) {
      if (error instanceof HttpException) {
        throw error;
      }
      console.error('Error marking attendance:', error);
      throw new HttpException('Error al marcar la asistencia: Error desconocido.', HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @Get()
  async findAll() {
    try {
      return await this.attendanceService.findAll();
    } catch (error) {
      throw new HttpException('Failed to get attendances', HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @Patch('update/:id')
  @Auth(Role.ADMIN)
  async updateById(@Param('id') id: string, @Body() updateAttendanceDto: UpdateAttendanceDto) {
    try {
      return await this.attendanceService.updateById(+id, updateAttendanceDto);
    } catch (error) {
      throw new HttpException('Failed to update attendance', HttpStatus.NOT_FOUND);
    }
  }

  @Get('weekly-hours')
async getWeeklyHours(
  @Query('email') email: string,
  @Query('weekOffset') weekOffset: string
) {
  if (!email) {
    throw new BadRequestException('Email is required');
  }

  const offset = parseInt(weekOffset, 10) || 0;
  const startDate = DateUtils.getStartOfWeek(new Date(), offset);
  const endDate = DateUtils.getEndOfWeek(startDate);

  return this.attendanceService.calculateWeeklyHours(startDate.toISOString(), endDate.toISOString(), email);
}

@Get('multi-weekly-hours')
@Roles(Role.ADMIN)
@UseGuards(RolesGuard)
async getMultiWeeklyHours(
  @Query('emails') emailsString: string,
  @Query('weekOffset') weekOffset: string,
  @Req() request: any
) {
  if (!emailsString) {
    throw new BadRequestException('Emails are required');
  }

  const emails = emailsString.split(',').map(email => email.trim());
  const offset = parseInt(weekOffset, 10) || 0;
  const currentDate = new Date();
  const startDate = DateUtils.getStartOfWeek(currentDate, offset);
  const endDate = DateUtils.getEndOfWeek(startDate);

  return this.attendanceService.calculateMultipleWeeklyHours(startDate.toISOString(), endDate.toISOString(), emails);
}

  @Get('monthly-hours')
  @Roles(Role.ADMIN)
  @UseGuards(AuthGuard, RolesGuard)
  async getMonthlyHours(
    @Query('email') email: string,
    @Query('year') year: string
  ) {
    if (!email) {
      throw new BadRequestException('Email is required');
    }

    if (!year) {
      throw new BadRequestException('Year is required');
    }

    const parsedYear = parseInt(year, 10);
    if (isNaN(parsedYear)) {
      throw new BadRequestException('Invalid year format');
    }

    return this.attendanceService.calculateMonthlyWorkHours(parsedYear, email);
  }

  @Get('monthly-multi-hours')
  @Roles(Role.ADMIN)
  @UseGuards(AuthGuard, RolesGuard)
  async getMonthlyMultiHours(
    @Query('emails') emails: string,
    @Query('year') year: string
  ) {
    if (!emails) {
      throw new BadRequestException('Emails are required');
    }
    if (!year) {
      throw new BadRequestException('Year is required');
    }

    const parsedYear = parseInt(year, 10);
    if (isNaN(parsedYear)) {
      throw new BadRequestException('Invalid year format');
    }

    const emailList = emails.split(',').map(email => email.trim());
    return this.attendanceService.calculateMonthlyWorkHoursForMultipleUsers(parsedYear, emailList);
  }

  @Get('weekly-summary')
  @Roles(Role.USER, Role.ADMIN)
  @UseGuards(AuthGuard, RolesGuard)
  async getWeeklySummary(
    @Query('weekOffset') weekOffset: string,
    @Req() req
  ) {
    const offset = parseInt(weekOffset, 10) || 0;
    const today = new Date();
    today.setDate(today.getDate() + offset * 7);
    const startOfWeek = new Date(today);
    startOfWeek.setDate(startOfWeek.getDate() - startOfWeek.getDay() + (startOfWeek.getDay() === 0 ? -6 : 1));
    startOfWeek.setHours(0, 0, 0, 0);

    const endOfWeek = new Date(startOfWeek);
    endOfWeek.setDate(endOfWeek.getDate() + 4); 
    endOfWeek.setHours(23, 59, 59, 999);

    let email = req.user.email;
    if (req.user?.roles && req.user.roles.includes(Role.ADMIN) && req.query.email) {
      email = req.query.email;
    }

    try {
      const attendances = await this.attendanceService.findAttendances(startOfWeek, endOfWeek, email);
      if (!attendances || Object.keys(attendances).length === 0) {
        return { message: 'No hay registros para esta semana' };
      }
      return attendances;
    } catch (error) {
      console.error('Error al procesar la solicitud:', error);
      throw new BadRequestException('Error interno del servidor');
    }
  }

  @Get('weekly-summary-email')
  @Auth(Role.ADMIN)
  @UseGuards(AuthGuard, RolesGuard)
  async getWeeklySummaryByEmail(
    @Query('weekOffset') weekOffset: string,
    @Query('email') email: string,
  ) {
    if (!email) {
      throw new BadRequestException('Email is required');
    }

    const offset = parseInt(weekOffset, 10) || 0;
    const today = new Date();
    const startOfWeek = DateUtils.getStartOfWeek(today, offset);
    const endOfWeek = DateUtils.getEndOfWeek(startOfWeek);

    try {
      const attendances = await this.attendanceService.findAttendancesByEmail(startOfWeek, endOfWeek, email);
      if (!attendances || Object.keys(attendances).length === 0) {
        return { message: 'No hay registros para esta semana' };
      }
      return attendances;
    } catch (error) {
      console.error('Error al procesar la solicitud:', error);
      throw new BadRequestException('Error interno del servidor');
    }
  }
}
