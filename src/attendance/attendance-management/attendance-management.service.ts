import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Attendance } from '../entities/attendance.entity';
import { CreateAttendanceDto } from '../dto/create-attendance.dto';
import { UpdateAttendanceDto } from '../dto/update-attendance.dto';


@Injectable()
export class AttendanceManagementService {
    constructor(
        @InjectRepository(Attendance) 
        private attendanceRepository: Repository<Attendance>
    ) {}

    async create(createAttendanceDto: CreateAttendanceDto, userEmail: string): Promise<Attendance> {
        const newAttendance = this.attendanceRepository.create({
            userEmail,
            timestamp: new Date(), 
            ...createAttendanceDto
        });
        return this.attendanceRepository.save(newAttendance);
    }

    async findAll(): Promise<Attendance[]> {
        return this.attendanceRepository.find();
    }

    async findOne(id: number): Promise<Attendance> {
        return this.attendanceRepository.findOneBy({ id });
    }

    async update(id: number, updateAttendanceDto: UpdateAttendanceDto): Promise<Attendance> {
        const attendance = await this.attendanceRepository.findOneBy({ id });
        if (!attendance) {
            throw new HttpException('Attendance not found', HttpStatus.NOT_FOUND);
        }
        Object.assign(attendance, updateAttendanceDto);
        return this.attendanceRepository.save(attendance);
    }

    async findByUserEmail(userEmail: string): Promise<Attendance[]> {
        return this.attendanceRepository.find({ where: { userEmail } });
    }
}
