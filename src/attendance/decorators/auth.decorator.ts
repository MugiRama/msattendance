import { UseGuards, applyDecorators } from '@nestjs/common';
import { AuthGuard } from '../guard/authAttendance.guard';

import { Roles } from './roles.decorator';
import { RolesGuard } from '../guard/roles.guard';

export function Auth(...roles: string[]) {
  return applyDecorators(
    Roles(...roles), 
    UseGuards(AuthGuard, RolesGuard)
  );
}
