import { IsEmail, IsNotEmpty, IsString, IsBoolean } from 'class-validator';

export class CreateAttendanceDto {
    @IsEmail()
    @IsNotEmpty()
    userEmail: string;

    @IsString()
    @IsNotEmpty()
    token: string;

    @IsBoolean()
    @IsNotEmpty()
    isEntry: boolean;  

    @IsNotEmpty()
    latitude: number;

    @IsNotEmpty()
    longitude: number;
}
