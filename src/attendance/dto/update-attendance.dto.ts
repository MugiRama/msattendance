import { IsBoolean, IsOptional, IsString } from 'class-validator';

export class UpdateAttendanceDto {
  @IsOptional()
  @IsString()
  timestamp?: string;

  @IsOptional()
  @IsBoolean()
  isEntry?: boolean;

  @IsBoolean()
  @IsOptional()
  isAdmin?: boolean;
}
