import { Injectable, CanActivate, ExecutionContext, ForbiddenException } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { ROLES_KEY } from '../decorators/roles.decorator';

@Injectable()
export class RolesGuard implements CanActivate {
  constructor(private reflector: Reflector) {}

  canActivate(context: ExecutionContext): boolean {
    const requiredRoles = this.reflector.getAllAndOverride<string[]>(ROLES_KEY, [
      context.getHandler(),
      context.getClass(),
    ]);
    const { user } = context.switchToHttp().getRequest();
  
    if (!user) {
      throw new ForbiddenException('No se encontró información de usuario');
    }
  
    console.log('Required roles:', requiredRoles);
    console.log('User role:', user.role);
  
    if (!requiredRoles.includes(user.role)) {
      throw new ForbiddenException('Recurso prohibido');
    }
    return true;
  }
  
}
