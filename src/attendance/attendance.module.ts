import { Module } from '@nestjs/common';
import { HttpModule } from '@nestjs/axios';  
import { TypeOrmModule } from '@nestjs/typeorm';
import { AttendanceService } from './attendance.service';
import { AttendanceController } from './attendance.controller';
import { Attendance } from './entities/attendance.entity';
import { UserService } from 'src/services/user.service';
import { JwtModule } from '@nestjs/jwt';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { AuthGuard } from './guard/authAttendance.guard';
import { RolesGuard } from './guard/roles.guard';
import { AttendanceValidationService } from './validation/attendance-validation.service'; 
import { AttendanceQueryService } from './attendance-query/attendance-query.service';

@Module({
  imports: [
    HttpModule,
    TypeOrmModule.forFeature([Attendance]),
    JwtModule.registerAsync({
      imports: [ConfigModule],
      useFactory: async (configService: ConfigService) => ({
        secret: configService.get<string>('JWT_SECRET'),
        signOptions: { expiresIn: '60m' },
      }),
      inject: [ConfigService],
    }),
    ConfigModule,
  ],
  controllers: [AttendanceController],
  providers: [
    AttendanceService, 
    UserService, 
    AuthGuard, 
    RolesGuard,
    AttendanceValidationService,
    AttendanceQueryService
  ],
})
export class AttendanceModule {}
