import { HttpException, HttpStatus, Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { MoreThanOrEqual, Repository } from "typeorm";
import { Attendance } from "../entities/attendance.entity";

@Injectable()
export class AttendanceValidationService {
    constructor(@InjectRepository(Attendance) private attendanceRepository: Repository<Attendance>) {}

    async validateDailyAttendance(userEmail: string, isEntry: boolean): Promise<void> {
        const startOfDay = new Date();
        startOfDay.setHours(0, 0, 0, 0);

        const attendancesToday = await this.attendanceRepository.find({
            where: {
                userEmail: userEmail,
                timestamp: MoreThanOrEqual(startOfDay),
            },
        });
        const hasEntryToday = attendancesToday.some(att => att.isEntry);
        const hasExitToday = attendancesToday.some(att => !att.isEntry);

        if (isEntry && hasEntryToday) {
            throw new HttpException('Ya has marcado tu entrada hoy.', HttpStatus.BAD_REQUEST);
        }
        if (!isEntry && !hasEntryToday) {
            throw new HttpException('No puedes marcar salida sin haber marcado entrada.', HttpStatus.BAD_REQUEST);
        }
        if (!isEntry && hasExitToday) {
            throw new HttpException('Ya has marcado tu salida hoy.', HttpStatus.BAD_REQUEST);
        }
    }
}
