import { Injectable, HttpException, HttpStatus, InternalServerErrorException } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Between, FindManyOptions, FindOptionsOrderValue, In, Repository } from "typeorm";
import { Attendance } from "./entities/attendance.entity";
import { CreateAttendanceDto } from "./dto/create-attendance.dto";
import { HttpService } from "@nestjs/axios";
import { UpdateAttendanceDto } from "./dto/update-attendance.dto";
import { UserService } from "src/services/user.service";
import { AttendanceValidationService } from "./validation/attendance-validation.service";
import { eachDayOfInterval } from "date-fns";
import { AttendanceQueryService } from "./attendance-query/attendance-query.service";

@Injectable()
export class AttendanceService {
    
    constructor(
        private httpService: HttpService,
        private userService: UserService,
        @InjectRepository(Attendance)
        private attendanceRepository: Repository<Attendance>,
        private validationService: AttendanceValidationService,
        private queryService: AttendanceQueryService
    ) {}

    async create(createAttendanceDto: CreateAttendanceDto, user: any): Promise<Attendance> {
        const { isEntry, latitude, longitude } = createAttendanceDto;
        const userEmail = user.email;
        await this.validationService.validateDailyAttendance(userEmail, isEntry);
        const newAttendance = this.attendanceRepository.create({
            userEmail,
            timestamp: new Date(),
            ...createAttendanceDto
        });
        return this.attendanceRepository.save(newAttendance);
    }

    async findAll(): Promise<Attendance[]> {
        return this.attendanceRepository.find();
    }

    async findOne(id: number): Promise<Attendance> {
        return this.attendanceRepository.findOneBy({ id });
    }

    async update(id: number, updateAttendanceDto: UpdateAttendanceDto): Promise<Attendance> {
        const attendance = await this.attendanceRepository.findOneBy({ id });
        if (!attendance) {
            throw new HttpException('Attendance not found', HttpStatus.NOT_FOUND);
        }
        Object.assign(attendance, updateAttendanceDto);
        return this.attendanceRepository.save(attendance);
    }

    async findByUserEmail(userEmail: string): Promise<Attendance[]> {
        return this.attendanceRepository.find({ where: { userEmail } });
    }
    async calculateMonthlyHours(year: number, month: number, email: string): Promise<any> {
        const startDate = new Date(year, month - 1, 1);
        const endDate = new Date(year, month, 0);

        const queryOptions: FindManyOptions<Attendance> = {
            where: {
                userEmail: email,
                timestamp: Between(startDate, endDate)
            },
            order: {
                timestamp: 'ASC'
            }
        };
        try {
            const attendances = await this.attendanceRepository.find(queryOptions);
            const totalHours = attendances.reduce((acc, record, index, array) => {
                if (record.isEntry && index + 1 < array.length && !array[index + 1].isEntry) {
                    const nextRecord = array[index + 1];
                    const duration = (nextRecord.timestamp.getTime() - record.timestamp.getTime()) / 3600000;
                    acc += duration;
                }
                return acc;
            }, 0);
            return { month, year, totalHours };
        } catch (error) {
            console.error('Failed to calculate monthly hours', error);
            throw new HttpException('Error calculating monthly hours', HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    async updateById(id: number, updateAttendanceDto: UpdateAttendanceDto): Promise<Attendance> {
        const attendanceToUpdate = await this.attendanceRepository.findOneBy({ id });
        if (!attendanceToUpdate) {
            throw new HttpException('Attendance not found', HttpStatus.NOT_FOUND);
        }

        Object.assign(attendanceToUpdate, updateAttendanceDto);
        attendanceToUpdate.isAdmin = updateAttendanceDto.isAdmin;
        return this.attendanceRepository.save(attendanceToUpdate);
    }
    async calculateWeeklyHours(startDate: string, endDate: string, email: string): Promise<any> {
        const startDateTime = new Date(startDate);
        startDateTime.setUTCHours(0, 0, 0, 0);

        const endDateTime = new Date(endDate);
        endDateTime.setUTCHours(23, 59, 59, 999);
        const queryOptions: FindManyOptions<Attendance> = {
            where: {
                timestamp: Between(startDateTime, endDateTime),
                userEmail: email
            },
            order: {
                timestamp: 'ASC'
            }
        };
        try {
            const hoursByDay: { [key: string]: { totalHours: number, entries: Attendance[] } } = {
                'Monday': { totalHours: 0, entries: [] },
                'Tuesday': { totalHours: 0, entries: [] },
                'Wednesday': { totalHours: 0, entries: [] },
                'Thursday': { totalHours: 0, entries: [] },
                'Friday': { totalHours: 0, entries: [] },
                'Saturday': { totalHours: 0, entries: [] },
                'Sunday': { totalHours: 0, entries: [] },
            };
            const attendances = await this.attendanceRepository.find(queryOptions);

            attendances.forEach(record => {
                const dayOfWeek = record.timestamp.getUTCDay();
                const dayName = this.getDayName(dayOfWeek);
                if (dayName) {
                    hoursByDay[dayName].entries.push(record);
                }
            });
            for (const day in hoursByDay) {
                const dayEntries = hoursByDay[day].entries;
                let totalHours = 0;

                for (let i = 0; i < dayEntries.length - 1; i++) {
                    if (dayEntries[i].isEntry && !dayEntries[i + 1].isEntry) {
                        const entryTime = new Date(dayEntries[i].timestamp).getTime();
                        const exitTime = new Date(dayEntries[i + 1].timestamp).getTime();
                        totalHours += (exitTime - entryTime) / 3600000;
                        i++;
                    }
                }
                hoursByDay[day].totalHours = totalHours;
            }
            return hoursByDay;
        } catch (error) {
            console.error('Failed to retrieve and calculate attendance hours', error);
            throw new HttpException('Error al recuperar y calcular las horas de asistencia', HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    private getDayName(day: number): string {
        const days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
        return days[day] || '';
    }
    async calculateMultipleWeeklyHours(startDate: string, endDate: string, emails: string[]): Promise<any> {
        const startDateTime = new Date(startDate);
        startDateTime.setUTCHours(0, 0, 0, 0);

        const endDateTime = new Date(endDate);
        endDateTime.setUTCHours(23, 59, 59, 999);
        const queryOptions: FindManyOptions<Attendance> = {
            where: {
                timestamp: Between(startDateTime, endDateTime),
                userEmail: In(emails)
            },
            order: {
                timestamp: 'ASC'
            }
        };
        try {
            const attendances = await this.attendanceRepository.find(queryOptions);
            const hoursByUser = emails.reduce((acc, email) => {
                acc[email] = { 'Monday': 0, 'Tuesday': 0, 'Wednesday': 0, 'Thursday': 0, 'Friday': 0, 'Saturday': 0, 'Sunday': 0 };
                return acc;
            }, {});
            for (let i = 0; i < attendances.length; i++) {
                const currentRecord = attendances[i];
                if (currentRecord.isEntry) {

                    const nextRecord = attendances.find((_, idx) => idx > i && !attendances[idx].isEntry && attendances[idx].userEmail === currentRecord.userEmail);
                    if (nextRecord) {
                        const entryTime = currentRecord.timestamp.getTime();
                        const exitTime = nextRecord.timestamp.getTime();
                        const hoursWorked = (exitTime - entryTime) / 3600000;
                        const dayOfWeek = currentRecord.timestamp.getUTCDay();
                        const dayName = this.getDayName(dayOfWeek);
                        if (dayName && hoursByUser[currentRecord.userEmail]) {
                            hoursByUser[currentRecord.userEmail][dayName] += hoursWorked;
                        }
                    }
                }
            }
            return hoursByUser;
        } catch (error) {
            console.error('Failed to retrieve and calculate attendance hours', error);
            throw new HttpException('Error al recuperar y calcular las horas de asistencia', HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    async calculateMonthlyWorkHours(year: number, email: string): Promise<{ [key: string]: number }> {
        const monthlyHours = {
            'January': 0, 'February': 0, 'March': 0, 'April': 0,
            'May': 0, 'June': 0, 'July': 0, 'August': 0,
            'September': 0, 'October': 0, 'November': 0, 'December': 0
        };

        for (let month = 0; month < 12; month++) {
            const startDate = new Date(year, month, 1);
            const endDate = new Date(year, month + 1, 0);

            const queryOptions = {
                where: {
                    userEmail: email,
                    timestamp: Between(startDate, endDate),
                },
                order: {
                    timestamp: 'ASC' as FindOptionsOrderValue,
                },
            };

            const attendances = await this.attendanceRepository.find(queryOptions);
            let totalHours = 0;

            const filteredAttendances = attendances.filter(record => {
                const dayOfWeek = record.timestamp.getUTCDay();
                return dayOfWeek !== 0 && dayOfWeek !== 6;
            });

            for (let i = 0; i < filteredAttendances.length - 1; i++) {
                if (filteredAttendances[i].isEntry && !filteredAttendances[i + 1].isEntry) {
                    const entryTime = filteredAttendances[i].timestamp;
                    const exitTime = filteredAttendances[i + 1].timestamp;
                    totalHours += (exitTime.getTime() - entryTime.getTime()) / 3600000; // Convertir milisegundos a horas
                    i++;
                }
            }

            const monthName = startDate.toLocaleString('en-US', { month: 'long' });
            monthlyHours[monthName] = totalHours;
        }

        return monthlyHours;
    }
    async calculateMonthlyWorkHoursForMultipleUsers(year: number, emails: string[]): Promise<{ [email: string]: { [key: string]: number } }> {
        const result = {};
        for (const email of emails) {
            result[email] = await this.calculateMonthlyWorkHours(year, email);
        }
        return result;
    }
    async findAttendances(startDate: Date, endDate: Date, email: string): Promise<any> {
        return this.queryService.findAttendances(startDate, endDate, email);
    }

    async findAttendancesByEmail(startDate: Date, endDate: Date, email: string): Promise<any> {
        return this.queryService.findAttendancesByEmail(startDate, endDate, email);
    }
}
