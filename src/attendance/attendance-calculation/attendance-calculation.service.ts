// attendance-calculation.service.ts
import { Injectable, HttpException, HttpStatus } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Between, FindManyOptions, Repository, FindOptionsOrderValue } from "typeorm";
import { Attendance } from "../entities/attendance.entity";

@Injectable()
export class AttendanceCalculationService {
    constructor(@InjectRepository(Attendance) private attendanceRepository: Repository<Attendance>) {}

    async calculateMonthlyHours(year: number, month: number, email: string): Promise<any> {
        const startDate = new Date(year, month - 1, 1);
        const endDate = new Date(year, month, 0);
        const queryOptions: FindManyOptions<Attendance> = {
            where: { userEmail: email, timestamp: Between(startDate, endDate) },
            order: { timestamp: 'ASC' as FindOptionsOrderValue }
        };
        try {
            const attendances = await this.attendanceRepository.find(queryOptions);
            return this.sumHours(attendances);
        } catch (error) {
            console.error('Failed to calculate monthly hours', error);
            throw new HttpException('Error calculating monthly hours', HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    private sumHours(attendances: Attendance[]): any {
        const totalHours = attendances.reduce((acc, record, index, array) => {
            if (record.isEntry && index + 1 < array.length && !array[index + 1].isEntry) {
                const nextRecord = array[index + 1];
                acc += (nextRecord.timestamp.getTime() - record.timestamp.getTime()) / 3600000;
            }
            return acc;
        }, 0);
        return { totalHours };
    }
}
