import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { HttpService } from '@nestjs/axios';
import { lastValueFrom } from 'rxjs';

@Injectable()
export class UserService {
  constructor(private httpService: HttpService) {}

  async verifyUser(token: string): Promise<any> {
    const verifyUrl = 'http://192.168.0.7:3000/api/v1/auth/validate-auth-token';
    try {
      const response = await lastValueFrom(this.httpService.post(verifyUrl, { token }));
      if (!response.data.isValid || !response.data.user) {
        throw new HttpException('Token inválido o información de usuario no disponible', HttpStatus.UNAUTHORIZED);
      }
      return response.data.user;
    } catch (error: any) {
      if (typeof error === 'object' && error !== null && error.response) {
        throw new HttpException(error.response.data.message, error.response.status);
      }
      throw new HttpException('Failed to verify token', HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}
